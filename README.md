# Same-Sex Relationship Experiences and Expectations Regarding Partnership and Parenthood

This was joined work with [Karsten Hank](http://www.iss-wiso.uni-koeln.de/de/institut/professuren/soziologie-iii-prof-dr-karsten-hank/). You find the article [here](https://www.demographic-research.org/volumes/vol39/default.htm). It was published in *Demographic Research* in 2018.

For an overview of this project [see here](https://b-dx.gitlab.io/).

### Structure
To obtain results from regression analyses shown in article:

Hank, Karsten & Wetzel, Martin (2018) "Same-Sex Relationship Experiences and Expectations Regarding Partnership and Parenthood" published in [Demographic Research Volume 39 - Article 25 (Pages 701–718)](https://www.demographic-research.org/volumes/vol39/25/default.htm).

1. Request pairfam-data from here: https://www.gesis.org/en/services/data-analysis/more-data-to-analyze/selected-german-research-projects/pairfam/

2. Use the "ms4044-same-sex.do" dofile written in Stata 14 for data preparation as well descriptive (Table 1) and inference analyses (Table).

3. After running the dofile, under /temp you find an excel file which shows all regression models.

4. And additional spider web graph visualizing the regression coefficents can also be found under /temp.



<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

You are free to:

  * Share — copy and redistribute the material in any medium or format
  * Adapt — remix, transform, and build upon the material
  * for any purpose, even commercially.

This license is acceptable for Free Cultural Works.

  * The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:

  * Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
  * ShareAlike — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
  * No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
