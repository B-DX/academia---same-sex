
************************************************************
************************************************************
***														 ***
***		    Replication File						   	 ***
***		Same-sex partnership experiences and 			 ***
***         expectations regarding partnership           ***
***         and parenthood              				 ***
***	  	Karsten Hank & Martin Wetzel 					 ***
***		ISS   University of Cologne April 2018           ***
***														 ***
************************************************************
************************************************************

***********************************************************
****   LOADING THE DATA      ************
***********************************************************

clear all
set more off		// tells Stata not to pause for --more-- messages

* Definition of a macro for the datapath with pairfam datasets
global datapath `"## add path here"'
global temp	    `"## add path here"'
cd "$datapath"		

use biopart, clear  //load partner episode data

	
	
***********************************************************
***** GENERATING EPISODE DATA ON UNION DURATION *****
***********************************************************

keep  id intdat dobp* meetp* sexp* begp* endp* 
reshape long dobp meetp sexp begp endp, i(id) j(partindex)

* no current partner
bys id: egen nocurpart  = total(partindex==0 & dobp==-3)
bys id: egen nopartev_h = total(inrange(begp,0,9999))
gen nopartev = nopartev_h == 0 & nocurpart == 1

tab nopartev nocurpart if id != id[_n-1]

********************************************
******* EHA WITH TIME-CONSTANT COVARIATES ************
********************************************

* Two time-constant covariates: cohort, country of birth
global keepvars bce1i1 - bce1i10 bce2i1 - bce2i10 isced dweight  ///
				relstat  homosex  marstat  nkids lfs cohort cob sex bula 

				
* Merge anchor1, pairfam main sample
merge m:1 id using anchor1, keepusing($keepvars)  
	drop _merge

mvdecode _all, mv(-11 / -1 = .a)
	
bys id (partindex):	gen N = _N
tab N if id != id[_n+1]

* Flagging homosexual experiences
sort id partindex

tab sexp sex, m
bys id: egen lg   = total(sex == sexp & !mi(sexp))
bys id: egen hete = total(sex != sexp & !mi(sexp))
gen bi = lg == 1 & hete == 1 
tab lg
keep if id != id[_n-1]
recode lg (0=0)(1/8=1)
tab2 lg homosex, m	
tab2 lg cohort, m	
*### grouping variable: lesbians&gays: lg
tab2 lg bi, m



* Country of birth: Federal Republic of Germany, German Democratic Republic, other
tab cob, m
recode cob -7=.a
recode cob 4/23=3



********************************************
******* PREPARTION ************
********************************************

recode relstat (1 6 9 = 0)   (2 3 4 5 7 8 10 11 = 1), gen(partner)
lab var partner "In partnerschaft?"
lab def partner_lb 0 "no" 	1 "yes"
lab val partner partner_lb

tab partner nocurpart

tab nocurpart nopartev

tab isced	
recode isced (0 1 2 3 = 1)(4 5 6 = 2) (7 8 = 3), gen(bil3)
recode nkids (0=0)(1/15=1), gen(child)
* recode migs (1 3 = 0) (1 = 2), gen(migr) 	
tab lfs
recode lfs (1 2 3 4 5 6 7 = 0) (8 9 10 11 12 13 = 1), gen(work)
recode bula (0/9 = 0) (10 / 16= 1), gen(westost)
mvdecode _all, mv(-11 / -1 = .a)

recode sex (1=0)(2=1)
recode cohort (1=-1)(2=0)(3=1)

********************************************
******* SELECTION ************
********************************************
* Cohort	
tab cohort, m
tab2 cohort lg, m row
drop if cohort == -1

d,s

* never had a partner
tab nopartev
tab2 nopartev cohort, m
drop if nopartev == 1			// 

tab lg, m
tab2 lg bi, m

tab bil3, m


********************************************
******* Analyses ************
********************************************

*####### VOC #######
*###################
*#### preparation ##
rename	bce2i1	vocpstim1
rename	bce2i2	vocpaff
rename	bce2i3	vocpest
rename	bce2i4	vocpcomf
rename	bce2i5	vocpstim2
		
rename	bce2i6	vocncomf1
rename	bce2i7	vocnaff
rename	bce2i8	vocncomf2
rename	bce2i9	vocnest
rename	bce2i10	vocnstim

mvdecode voc*, mv(7 = .b)

egen mi = rowmiss(sex nocurpart child work cohort bil3 cob)
bro id sex nocurpart child work cohort bil3 cob lg if mi > 0

tab mi, m
 
drop partner
recode nocurpart (1=0)(0=1), gen(partner)

tab lg partner
tab lg homosex
tab lg bi if partner == 0

bys lg: tab sex sexp

*gen g = (partner == 1 & homosex == 1) | (partner == 0 & lg == 1)
*tab g

*drop lg
*rename g lg

* drop if bil3 == .a

*#### benefits
global voc vocnaff vocnstim vocpcomf vocncomf1

eststo clear
foreach v of varlist $voc {
	qui eststo `v'a: regress `v' c.lg 
	qui eststo `v'b: regress `v' c.lg c.sex partner child work cohort i.bil3 ///
							i.cob
	qui eststo `v'c: regress `v' c.lg##c.sex partner child work cohort i.bil3 ///
							i.cob
	qui eststo `v'd: regress `v' c.lg c.sex partner work cohort i.bil3 ///
							i.cob
	count if !mi(`v') & lg == 1	
	// Missing auf AV nach sex-Praeferenz
	tab2 `v' lg, m col
	// Missing auf Kontrollvariable nach sex-Praeferenz
	tab2 `v' lg if !mi(`v') & mi != 0, m 	
	}
estout vocnaffa vocnstima vocpcomfa vocncomf1a , cells(b(star fmt(2)) se(par fmt(2))) ///
   legend  varlabels(_cons Constant) stats(r2 N)

estout vocnaffb vocnstimb vocpcomfb vocncomf1b, cells(b(star fmt(2)) se(par fmt(2))) ///
   legend  varlabels(_cons Constant) stats(r2 N) 

estout vocnaffd vocnstimd vocpcomfd vocncomf1d, cells(b(star fmt(2)) se(par fmt(2))) ///
   legend  varlabels(_cons Constant) stats(r2 N) 
   
estout *, cells(b(star fmt(2)) p(par fmt(2))) ///
   legend  varlabels(_cons Constant) stats(r2 N) ///
   starlevels(+ 0.10 * 0.05 ** 0.01 *** 0.001)  stardetach 
   
estout * using "$temp\tab_vocp.csv", cells(b(star fmt(2)) se(par fmt(2))) ///
   legend  varlabels(_cons Constant) stats(r2 N, fmt(%9.2f %9.0g)) ///
   starlevels(+ 0.10 * 0.05 ** 0.01 *** 0.001)  stardetach replace

d $voc  
 

*####### VOP #######
*###################
*#### prepartions ##

rename	bce1i1	voppstim
rename	bce1i2	voppcomf1
rename	bce1i3	voppstat
rename	bce1i4	voppcomf2
rename	bce1i5	voppaff
rename	bce1i6	voppauto

rename	bce1i7	vopncomf
rename	bce1i8	vopnstim
rename	bce1i9	vopnstat
rename	bce1i10	vopnauton
* rename	bce1i11	vopnaff


mvdecode vop*, mv(7 = .b)

global vop voppcomf1 vopnauton voppaff vopnstat

eststo clear
foreach v of varlist $vop {
	qui eststo `v'a: regress `v' c.lg 
	qui eststo `v'b: regress `v' c.lg c.sex partner child work cohort i.bil3 ///
							i.cob
	qui eststo `v'c: regress `v' c.lg##c.sex partner child work cohort i.bil3 ///
							i.cob
	qui eststo `v'd: regress `v' c.lg c.sex partner work cohort i.bil3 ///
							i.cob
	count if !mi(`v') & lg == 1	
	// Missing auf AV nach sex-Praeferenz
	tab2 `v' lg, m col
	// Missing auf Kontrollvariable nach sex-Praeferenz
	tab2 `v' lg if !mi(`v') & mi != 0, m 	
	}
	
estout voppcomf1a vopnautona voppaffa vopnstata, cells(b(star fmt(2)) se(par fmt(2))) ///
   legend  varlabels(_cons Constant) stats(r2 N) ///
   starlevels(+ 0.10 * 0.05 ** 0.01 *** 0.001)
 
estout voppcomf1d vopnautond voppaffd vopnstatd, cells(b(star fmt(2)) se(par fmt(2))) ///
   legend  varlabels(_cons Constant) stats(r2 N) ///
   starlevels(+ 0.10 * 0.05 ** 0.01 *** 0.001)
 
estout * using "$temp\tab_vopp.csv", cells(b(star fmt(2)) se(par fmt(2))) ///
   legend  varlabels(_cons Constant) stats(r2 N, fmt(%9.2f %9.0g)) replace  ///
   starlevels(+ 0.10 * 0.05 ** 0.01 *** 0.001)  stardetach

d    $vop

   
**********
* TTests
for any vocpaff vocpest vocpcomf vocpstim1 vocpstim2 : ttest X, by(lg)
for any vocnaff vocnest vocncomf1 vocncomf2 vocnstim : ttest X, by(lg)
for any $vop: ttest X, by(lg)
	
 
 
*######### nice Appendix figure 1 #########

set scheme lean2

egen slg = group(lg sex)

*local vars vocpaff vocpest vocpcomf vocpstim1 vocpstim2 
*local vars vocnaff vocnest vocncomf1 vocncomf2 vocnstim 
*local vars voppaff voppcomf1 voppcomf2 voppstim voppstat voppauto
local vars vopncomf	vopnstim vopnstat vopnauton


global grli ""
foreach v of varlist `vars' {
preserve
	collapse (mean) m`v'=`v' (sd) s`v'=`v' (count) n`v'=`v' , by(slg)
	list _all
	gen `v'uci = m`v' + invttail(n`v', 0.025)*s`v'/sqrt(n`v')
	gen `v'lci = m`v' - invttail(n`v', 0.025)*s`v'/sqrt(n`v')	
	twoway (bar m`v' slg, barwidth(.5))(rcap `v'uci `v'lci slg, blwid(medthick) ///
			msize(large) xlabel(1(1)4)), name(g`v', replace) leg(off) t("`v'") ///
			 ylabel(1(0.5)5) nodraw
	global grli $grli g`v'	
	list
restore
	}
	graph combine $grli , row(1) ycommon
	graph export "$temp\vopn.png", replace 
 
 save "$temp\samesex_1.dta", replace
 
*######### table 1 ############## 
*######### descriptives #########
set dp comma
tabstat $vop , s(mean sd n min max) by(lg) format(%8.2f)
tab bil3, gen(b_)
tab cob, gen(c_)
tabstat sex partner child work cohort b_* c_* , s(mean n) by(lg)
egen lgsex = group(lg sex)
tabstat $vop $voc sex partner child work cohort b_* c_* , s(mean n) by(lgsex)

tabstat $vop $voc, s(mean n) by(lg)
 
*####### Models without people with bi-sexual orientation ######
local vars1 vocpaff vocpest vocpcomf vocpstim1 vocpstim2 
local vars2 vocnaff vocnest vocncomf1 vocncomf2 vocnstim 
local vars3 voppaff voppcomf1 voppcomf2 voppstim voppstat voppauto
local vars4 vopncomf vopnstim vopnstat vopnauton

eststo clear
foreach v of varlist `vars1' `vars2' `vars3' `vars4' {
	qui eststo `v'a: regress `v' lg sex partner child work cohort i.bil3 ///
							i.cob 
	qui eststo `v'b: regress `v' lg sex partner child work cohort i.bil3 ///
							i.cob if bi != 1
	}
estout * using "$temp\tab_wout_bi.csv", cells(b(star fmt(2)) se(par fmt(2))) ///
   legend  varlabels(_cons Constant) stats(r2 N, fmt(%9.2f %9.0g)) replace  ///
   starlevels(+ 0.10 * 0.05 ** 0.01 *** 0.001)  stardetach

