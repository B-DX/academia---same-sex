**READ ME**

To obtain results from regression analyses shown in article: 
Hank, Karsten & Wetzel, Martin (2018) "Same-Sex Relationship Experiences and Expectations Regarding Partnership and Parenthood".

1. Request pairfam-data from here: https://www.gesis.org/en/services/data-analysis/more-data-to-analyze/selected-german-research-projects/pairfam/

2. Use the "ms4044-same-sex.do" dofile written in Stata 14 for data preparation as well descriptive (Table 1) and inference analyses (Table). 

3. Under /temp you find an excel file which shows all regression models.

4. And additional spider web graph visualizing the regression coefficents can also be found under /temp. 